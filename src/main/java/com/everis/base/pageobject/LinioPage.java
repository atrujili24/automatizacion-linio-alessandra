package com.everis.base.pageobject;


/**
 * @author jovallep
 */
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.linio.com.pe/")
public class LinioPage extends PageObject {

    public void unUsuarioNuevoIngresaALaPáginaWebDeLinio() {
    }

    @FindBy(xpath = "//*[@id=\"navbar\"]/div[2]/div[3]/div[1]/a[3]/span")
    public WebElementFacade btnUsuario;

    @FindBy(xpath = "//*[@id=\"register-form\"]/form/button")
    public WebElementFacade btnIniciarSesión;

    @FindBy(xpath = "/html/body/div[1]/main/div/ul/li[2]")
    public WebElementFacade btnCrearCuenta;

    @FindBy(xpath = "//*[@id=\"registration_firstName\"]")
    public WebElementFacade txtNombre;

    @FindBy(xpath = "//*[@id=\"registration_lastName\"]")
    public WebElementFacade txtApellidos;

    @FindBy(xpath = "//*[@id=\"registration_email\"]")
    public WebElementFacade txtEmail;

    @FindBy(xpath = "//*[@id=\"registration_password\"]")
    public WebElementFacade txtContraseña;

    @FindBy(xpath = "//*[@id=\"registration_nationalRegistrationNumber\"]")
    public WebElementFacade txtDNI;

    @FindBy(xpath = "//*[@id=\"register-form\"]/form/label[2]")
    public WebElementFacade btnActivarPoliticaDePrivacidad;

    @FindBy(xpath = "")
    public WebElementFacade textoCrearCuenta;

}
