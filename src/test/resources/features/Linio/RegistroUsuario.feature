Feature: Registro en la web de Linio

  @imperativo
  Scenario: Usuario nuevo en Linio
    Given un usuario nuevo ingresa a la página web de Linio
    When selecciono el boton usuario
    And selecciono Iniciar sesión
    And selecciono Crear cuenta
    And ingresa nombre "Carlos"
    And ingresa apellidos "Cruz López"
    And ingresa email "prueba123@gmail.com"
    And ingresa contraseña "CCL123456789++"
    And ingresa DNI "75395146"
    And seleccionar activar politica de privacidad
    Then validar el texto del boton "Completar registro"

