package com.everis.base.stepDefinitions;

import com.everis.base.steps.LinioSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LinioStepDefinition {

    @Steps
    public LinioSteps linioSteps;

    @Given("un usuario nuevo ingresa a la página web de Linio")
    public void unUsuarioNuevoIngresaALaPáginaWebDeLinio() {
        linioSteps.unUsuarioNuevoIngresaALaPáginaWebDeLinio();
    }

    @When("selecciono el boton usuario")
    public void seleccionoElBotonUsuario() {
        linioSteps.seleccionoElBotonUsuario();
    }

    @And("selecciono Iniciar sesión")
    public void seleccionoIniciarSesión() {
        linioSteps.seleccionoIniciarSesión();
    }

    @And("selecciono Crear cuenta")
    public void seleccionoCrearCuenta() {
        linioSteps.seleccionoCrearCuenta();
    }

    @And("ingresa nombre {string}")
    public void ingresaNombre(String var) {
        linioSteps.ingresaNombre(var);
    }

    @And("ingresa apellidos {string}")
    public void ingresaApellidos(String var) {
        linioSteps.ingresaApellidos(var);
    }

    @And("ingresa email {string}")
    public void ingresaEmail(String var) {
        linioSteps.ingresaEmail(var);
    }

    @And("ingresa contraseña {string}")
    public void ingresaContraseña(String var) {
        linioSteps.ingresaContraseña(var);
    }

    @And("ingresa DNI {string}")
    public void ingresaDNI(String var) {
        linioSteps.ingresaDNI(var);
    }

    @And("seleccionar activar politica de privacidad")
    public void seleccionarActivarPoliticaDePrivacidad() {
        linioSteps.seleccionarActivarPoliticaDePrivacidad();
    }

    @Then("^validar el texto del boton \"([^\"]*)\"$")
    public void validareltextodelbotonCompletarregistro(String texto){
        linioSteps.validareltextodelbotonCompletarregistro(texto);
    }
}