package com.everis.base.steps;

import com.everis.base.pageobject.LinioPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jovallep
 */
public class LinioSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(LinioSteps.class);

    private LinioPage page;

    @Step
    public void crearCliente() {
        LOGGER.info("Ingresa la información del cliente");
        seleccionoCrearCuenta();
        ingresaNombre("000");
        ingresaApellidos("000");
        ingresaEmail("asd@asd.com");
        ingresaContraseña("123456");
        ingresaDNI("789546321");
    }

    public void unUsuarioNuevoIngresaALaPáginaWebDeLinio() {
        LOGGER.info("Abre la pagina web");
        page.unUsuarioNuevoIngresaALaPáginaWebDeLinio();
    }

    public void seleccionoElBotonUsuario() {
        LOGGER.info("Seleccionar el boton Usuario");
        page.btnUsuario.click();
    }

    public void seleccionoIniciarSesión() {
        LOGGER.info("Seleccionar Iniciar Sesión");
        page.btnIniciarSesión.click();
    }

    public void seleccionoCrearCuenta() {
        LOGGER.info("Seleccionar el boton Crear Cuenta");
        page.btnCrearCuenta.click();
    }

    public void ingresaNombre(String var) {
        LOGGER.info("Escribe el Nombre");
        page.txtNombre.sendKeys(var);
    }

    public void ingresaApellidos(String var) {
        LOGGER.info("Escribe el Apellido");
        page.txtApellidos.sendKeys(var);
    }

    public void ingresaEmail(String var) {
        LOGGER.info("Escribe el Correo");
        page.txtEmail.sendKeys(var);
    }

    public void ingresaContraseña(String var) {
        LOGGER.info("Escribe el Contraseña");
        page.txtContraseña.sendKeys(var);
    }

    public void ingresaDNI(String var) {
        LOGGER.info("Escribe el DNI");
        page.txtDNI.sendKeys(var);
    }

    public void seleccionarActivarPoliticaDePrivacidad() {
        LOGGER.info("Seleccionar Activar Política Privacidad");
        page.btnActivarPoliticaDePrivacidad.click();
    }

    public void validareltextodelbotonCompletarregistro(String texto) {
        LOGGER.info("texto:" + page.textoCrearCuenta.getText());
        page.textoCrearCuenta.isDisplayed();
        Assert.assertEquals(texto, page.textoCrearCuenta.getText());
    }


}
